import React from 'react';
import { Link } from 'react-router-dom';
import Avatar, { AvatarItem } from '@atlaskit/avatar';

type Character = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
  modified: string;
}

type Thumbnail = {
  path: string;
  extension: string;
}

type Props = {
  characters: Character[]
}

const Heroes = ({ characters }: Props): JSX.Element => {
  return (
    <div className="heros__wrapper">
      {characters.map((item) => (
        <div key={item.id} className="heros__info">
          <div>
            <AvatarItem
              backgroundColor="rgb(102, 27, 27)"
              avatar={<Avatar
                appearance="square"
                src={`${item.thumbnail.path}/standard_large.jpg`}
                size="xlarge"
              />}
              primaryText={item.name}
              secondaryText={`Modified <<<<<${item.modified}>>>>> ${item.description}`}
            />
          </div>
          <Link to={`/hero/${item.id}`}>See more</Link>
        </div>
      ))
      }
    </div>
  )
}

export default Heroes
