import React from 'react';
import { RouteComponentProps } from 'react-router';
import thanos from '../assets/imgs/Thanos.jpg';

function Error(props: RouteComponentProps): JSX.Element {
  return (
    <div>
      <button type="button" onClick={()=> props.history.push(`/search`)}>Back</button>
      <h1>404.Not found</h1>
      <img src={thanos} alt="Thanos"/>
    </div>
  )
}

export default Error
