import React from 'react'
import { RouteComponentProps } from 'react-router-dom';

type Comics = {
  comics: Comic[]
}

type Comic = {
  id: number;
  thumbnail: Thumbnail;
  description: string;
  prices: Prices[];
  digitalId: number;
}

type Prices = {
  type: string;
  price: number;
}

type Thumbnail = {
  path: string;
  extension: string;
}

const ComicCard = (props: RouteComponentProps & Comics): JSX.Element => {
console.log(props.comics)
  return (
      <div className="comics__info">
        <button type="button" onClick={() => props.history.goBack()}>Back</button>
        {props.comics.map(comic =>
          <div key={comic.id} className="comic">
            <img src={`${comic.thumbnail.path}/portrait_xlarge.jpg`} alt="heros`s photo" />
            <div className="comic_description">{comic.description}</div>
            <div>
              {comic.prices.map(prices =>
                <div key={prices.type + comic.id}>{prices.type}...{prices.price}</div>
              )}
            </div>
          </div>
        )}
      </div>
  )
}

export default ComicCard
