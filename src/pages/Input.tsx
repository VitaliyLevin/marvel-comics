import React from 'react';

import Textfield from '@atlaskit/textfield';

type InputProps = {
  handlerInput: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
}

const Input = ({ value, handlerInput }: InputProps): JSX.Element => {
  return (
    <Textfield
      name="basic"
      value={value}
      aria-label="Enter the name of the hero"
      placeholder="Enter the name of the hero "
      onChange={handlerInput}
      appearance='subtle'
      css={{
        flex: '1 1 526px',
        marginRight: '10px',
        height: '30px',
        border: '2px solid #fff',
        '& > [data-ds--text-field--input]::placeholder': {
          color: '#fff',
          textTransform: 'uppercase',
        },
        ':focus': {
          border: '2px solid rgb(102, 27, 27)'
        },
        ':focus-within': {
          border: '2px solid rgb(102, 27, 27)'
        }
      }}
    />
  )
}

export default Input
