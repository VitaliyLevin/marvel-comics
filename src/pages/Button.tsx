import React from 'react';
import Button from '@atlaskit/button';
import { RouteComponentProps } from 'react-router-dom';

type StyledButtonProps = {
  pagination: (pageNumber: number) => void;
  pageNumber: number;
}

const StyledButton = (props: RouteComponentProps & StyledButtonProps): JSX.Element => {
  const page = new URLSearchParams(props.location.search).get('page')
  const checkPage = page === null ? 1 : page
  const color = props.pageNumber === +checkPage ? 'rgb(102, 27, 27)': '';
  return (
    <div className={color ? "pagination__btn__active" : ''}>
      <Button
        onClick={() => props.pagination(props.pageNumber)}
      >
        {props.pageNumber}
      </Button>
    </div>
  )
}

export default StyledButton;
