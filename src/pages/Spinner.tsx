import React from 'react';

import Spinner from '@atlaskit/spinner';

 const XlargeSpinner = (): JSX.Element => <Spinner size="xlarge"/>;

 export default XlargeSpinner