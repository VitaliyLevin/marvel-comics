import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import Heroes from '../pages/Heroes';
import XlargeSpinner from '../pages/Spinner';
import { getHeroes } from '../assets/apis/apis';
import Input from '../pages/Input';
import Pagination from '@atlaskit/pagination';
import StyledButton from '../pages/Button'


interface State {
  inputValue: string;
  characters: Character[];
  loader: boolean;
  total: number;
}

type Character = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
  modified: string
}

type Thumbnail = {
  path: string;
  extension: string;
}

type Params = {
  name: string;
  page: number;
  orderBy: string;
}

class Search extends Component<RouteComponentProps, State> {
  state: Readonly<State> = {
    inputValue: '',
    characters: [],
    loader: false,
    total: 0,
  }

  public componentDidMount = (): void => {
    const { name, page, orderBy } = this.getSearchParams()
    if (name) {
      this.setState({
        inputValue: name,
        loader: true,
      }, () => this.fetchHeroes(this.state.inputValue, +page, orderBy))
    }
  }

  private getSearchParams = (): Params => {
    let paramsObj = {
      name: '',
      page: 0,
      orderBy: ''
    }
    const params = new URLSearchParams(this.props.location.search)
    params.forEach((value, key) =>
      paramsObj = { ...paramsObj, [key]: value }
    )
    return paramsObj;
  }

  private handlerInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value }: { value: string } = e.target;
    this.setState({
      inputValue: value
    })
  }

  private sortByModified = (): void => {
    const { name, page, orderBy } = this.getSearchParams();
    const orderByModified = orderBy === 'modified' ? '-modified' : 'modified';
    const url = page === 0 ? (`/search/?name=${name}&orderBy=${orderByModified}`
    ) : (
      `/search/?name=${name}&page=${page}&orderBy=${orderByModified}`);
    this.props.history.push(url);
  }

  private startSearch = (): void => {
    const { inputValue } = this.state
    if (this.state.inputValue) {
      this.setState({
        loader: true,
      }, () => {
        this.props.history.push(`/search/?name=${inputValue}&orderBy=modified`)
      })
    }
  }

  private pagination = (pageNumber: number): void => {
    const { name, orderBy } = this.getSearchParams();
    const url = pageNumber === 1 ? (
      `/search/?name=${name}&orderBy=${orderBy}`
    ) : (
      `/search/?name=${name}&page=${pageNumber}&orderBy=${orderBy}`);
    this.props.history.push(url);
  }

  private fetchHeroes = (name: string, page: number, sort?: string): void => {
    const limit = 3;
    const offset = page === 0 ? page * 3 : (page - 1) * 3;
    getHeroes(name, limit, offset, sort)
      .then(response => {
        const characters: Character[] = response.data.results;
        const total: number = response.data.total;
        if (characters.length === 0) {
          this.props.history.push('/error');
        }
        this.setState({
          characters: characters,
          inputValue: name,
          loader: false,
          total: total,
        })
      });
  }

  public componentDidUpdate = (prevProps: RouteComponentProps): void => {
    const { name, page, orderBy } = this.getSearchParams();
    if (prevProps.location.key !== this.props.location.key && name) {
      this.fetchHeroes(name, +page, orderBy);
    }
  }

  public render(): JSX.Element {
    const {
      inputValue,
      characters,
      loader,
      total,
    } = this.state;
    const pages = (new Array(Math.ceil(total / 3)).fill(1).map((a, i) => i + 1))
    const { page } = this.getSearchParams();
    return (
      <>
        {loader && <div className="spinner"><XlargeSpinner /></div>}
        <div className="background__img">
          <div className="wrapper">
            <div className="search">
              <Input
                value={inputValue}
                handlerInput={this.handlerInput}
              />
              <button type="button" onClick={this.sortByModified}>Sort by modified</button>
              <button type="button" onClick={this.startSearch}>Search</button>
            </div>
            <div>
              {!loader &&
                <Heroes characters={characters} />
              }
            </div>
            {characters.length !== 0 &&
              <div className="pagination__btns">
                <Pagination
                  components={{
                    Page: (props) => {
                      return <StyledButton
                        {...this.props}
                        pageNumber={props.page}
                        pagination={this.pagination}
                      />
                    }
                  }}
                  pages={pages}
                  onChange={(event: React.SyntheticEvent<Event>, page: number) => {
                    this.pagination(page)
                  }}
                  selectedIndex={page === 0 ? page : page - 1}
                />
              </div>
            }
          </div>
        </div>
      </>
    )
  }
}

export default Search
