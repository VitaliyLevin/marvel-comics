import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router';
import XlargeSpinner from '../pages/Spinner';
import ComicCard from '../pages/ComicCard';
import { getComics } from '../assets/apis/apis';

interface State {
  comics: Comic[];
  loader: boolean;
}

type Comic = {
  id: number;
  thumbnail: Thumbnail;
  description: string;
  prices: Prices[];
  digitalId: number;
}

type Prices = {
  type: string;
  price: number;
}

type Thumbnail = {
  path: string;
  extension: string;
}

type ParamsObj = {
  id: string
}


class Comics extends Component<RouteComponentProps<ParamsObj>, State>{

  state: Readonly<State> = {
    comics: [],
    loader: true
  }

  componentDidMount = (): void => {
    const { match } = this.props
    this.fetchComics(Number(match.params.id))
  }

  private fetchComics = (id: number | null): void => {
    getComics(id)
      .then(res => {
        const comics: Comic[] = res.data.results;
        this.setState({
          comics: comics,
          loader: false
        })
      })
  }

  public render(): JSX.Element {
    const { comics, loader }: { comics: Comic[], loader: boolean } = this.state
    return (
      <>
        {loader && <div className="spinner"><XlargeSpinner /></div>}
        {!loader && <ComicCard {...this.props} comics={comics} />}
      </>
    )
  }
}

export default Comics
