import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Search from './components/Search';
import Comics from './components/Comics';
import Error from './pages/Error';


const App = (): JSX.Element => {
  return (
    <Router>
      <Switch>
        <Route path='/search' exact component={Search} />
        <Route path='/error' component={Error} />
        <Route path='/hero/:id' exact component={Comics} />
      </Switch>
    </Router>
  );
};

export default App;
