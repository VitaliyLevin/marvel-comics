import axios from 'axios';

type ResponseDataHeroes = {
  data: HeroesData;
}

type HeroesData = {
  results: Character[];
  total: number;
  offset: number;
}

type Character = {
  id: number;
  thumbnail: Thumbnail;
  name: string;
  description: string;
  modified: string
}

type Thumbnail = {
  path: string;
  extension: string;
}

type ResponseDataComics = {
  data: ComicsData;
}

type ComicsData = {
  results: Comic[];
}

type Comic = {
  id: number;
  thumbnail: Thumbnail;
  description: string;
  prices: Prices[];
  digitalId: number;
}

type Prices = {
  type: string;
  price: number;
}


export const getHeroes = async (
  nameStartsWith: string,
  limit: number,
  offset: number,
  orderBy?: string,
  name?: string,
): Promise<ResponseDataHeroes> => {
  try {
    const response = await axios.get<ResponseDataHeroes>('https://gateway.marvel.com:443/v1/public/characters', {
      params: {
        name: name,
        nameStartsWith: nameStartsWith,
        limit: limit,
        offset: offset,
        orderBy: orderBy,
        apikey: 'e301b0fee4c42f22636419298243a633'
      }
    });
    return response.data;
  } catch (err) {
    console.error(err);
    throw new Error('Error');
  }
};

export const getComics = async (id: number | null): Promise<ResponseDataComics> => {
  try {
    const response = await axios.get<ResponseDataComics>(`https://gateway.marvel.com:443/v1/public/characters/${id}/comics`, {
      params: {
        apikey: 'e301b0fee4c42f22636419298243a633'
      }
    });
    return response.data;
  } catch (err) {
    throw new Error('Error')
  }
}
